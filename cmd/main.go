package main

import (
	"applicationDesignTest/internal/di"
	"applicationDesignTest/internal/services"
	"fmt"
	"log"
	"log/slog"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

type App struct {
	logger *slog.Logger
}

func NewApp() *App {
	return &App{
		logger: slog.New(slog.NewJSONHandler(os.Stdout, nil)),
	}
}

func main() {
	if err := NewApp().Run(); err != nil {
		log.Fatal(err)
	}
}

func (a *App) Run() error {
	config := di.DIConfig{
		Webaddr:     ":8000",
		Logger:      a.LogInfo,
		ErrorLogger: a.LogError,
	}
	parseFlags(&config)
	deps := di.NewDIContainer(config)

	s := []services.Service{
		deps.WebService(),
	}

	var wg sync.WaitGroup

	for _, srv := range s {
		wg.Add(1)
		s := srv
		go func() {
			err := s.Run()
			if err != nil {
				a.LogError(err)
			}
		}()
	}

	stopCh := make(chan os.Signal, 1)
	signal.Notify(stopCh, syscall.SIGINT, syscall.SIGTERM)
	select {
	case <-stopCh:
		fmt.Println("Received interrupt signal. Shutting down...")

		for _, server := range s {
			if err := server.Shutdown(); err != nil {
				fmt.Println("Error shutting down service:", err)
			}
			wg.Done()
		}
		wg.Wait()

		fmt.Println("All services gracefully stopped.")
	}
	return nil
}

func parseFlags(config *di.DIConfig) {
	//if c.String(apikeyFlag) != "" {
	//	config.Apikey = c.String(apikeyFlag)
	//}
}

func (a *App) LogError(err error) {
	msg := err.Error()
	a.logger.Error(msg)
}

func (a *App) LogInfo(format string, v ...any) {
	if len(v) == 0 {
		a.logger.Info(format)
		return
	}
	msg := fmt.Sprintf(format, v...)
	a.logger.Info(msg)
}
