package data

import (
	"applicationDesignTest/internal/entities"
	"applicationDesignTest/internal/entities/proxies"
)

type Store interface {
	GetByEmail(string) ([]proxies.OrderProxy, error)

	Book(booking entities.Booking) (proxies.OrderProxy, error)
}
