package data

import (
	"applicationDesignTest/internal/converters"
	"applicationDesignTest/internal/entities"
	"applicationDesignTest/internal/entities/proxies"
	"applicationDesignTest/internal/weberrors"
	"sync"
	"time"
)

var _ Store = (*MemoryStore)(nil)

type MemoryStore struct {
	orders []proxies.OrderProxy
	m      sync.RWMutex
}

func (store *MemoryStore) GetByEmail(userEmail string) ([]proxies.OrderProxy, error) {
	store.m.RLock()
	defer store.m.RUnlock()
	res := make([]proxies.OrderProxy, 0)
	for _, item := range store.orders {
		if item.UserEmail == userEmail {
			res = append(res, item)
		}
	}
	return res, nil
}

func (store *MemoryStore) Book(b entities.Booking) (proxies.OrderProxy, error) {
	store.m.RLock()
	for _, order := range store.orders {
		if b.From.Before(order.To) && b.To.After(order.From) {
			store.m.RUnlock()
			return proxies.OrderProxy{}, weberrors.ErrConflict
		}
	}
	store.m.RUnlock()

	store.m.Lock()
	defer store.m.Unlock()
	successBooking := converters.FromBookingToProxy(b)
	store.orders = append(store.orders, successBooking)
	return successBooking, nil
}

func NewMemoryStore() *MemoryStore {
	return &MemoryStore{
		orders: []proxies.OrderProxy{
			{Room: "lux", UserEmail: "test", From: time.Date(2023, time.January, 1, 0, 0, 0, 0, time.UTC), To: time.Date(2023, time.December, 1, 0, 0, 0, 0, time.UTC)},
		},
		m: sync.RWMutex{},
	}
}
