package converters

import (
	"applicationDesignTest/internal/entities"
	"applicationDesignTest/internal/entities/proxies"
)

func FromOrderProxy(o proxies.OrderProxy) entities.Order {
	return entities.Order{
		Room:      o.Room,
		UserEmail: o.UserEmail,
		From:      o.From.String(),
		To:        o.To.String(),
	}
}

func FromBookingToProxy(booking entities.Booking) proxies.OrderProxy {
	return proxies.OrderProxy{
		Room:      booking.Room,
		UserEmail: booking.UserEmail,
		From:      booking.From,
		To:        booking.To,
	}
}
