package usecases

import (
	"applicationDesignTest/internal/converters"
	"applicationDesignTest/internal/data"
	"applicationDesignTest/internal/entities"
	"applicationDesignTest/internal/entities/definitions"
	"errors"
)

type OrderUseCases struct {
	store  data.Store
	logger definitions.LoggerFunc
	error  definitions.ErrorLoggerFunc
}

func (u *OrderUseCases) GetByEmail(email string) ([]entities.Order, error) {
	res, err := u.store.GetByEmail(email)
	if err != nil {
		return nil, errors.New("error")
	}
	out := make([]entities.Order, 0, len(res))
	for _, proxy := range res {
		out = append(out, converters.FromOrderProxy(proxy))
	}
	return out, nil
}

func (u *OrderUseCases) Book(booking entities.Booking) (entities.Order, error) {
	proxy, err := u.store.Book(booking)
	if err != nil {
		return entities.Order{}, err
	}
	return converters.FromOrderProxy(proxy), nil
}
