package usecases

import (
	"applicationDesignTest/internal/data"
	"applicationDesignTest/internal/entities/definitions"
)

type UseCasesContainer struct {
	store       data.Store
	orderUC     *OrderUseCases
	webLogger   definitions.LoggerFunc
	errorLogger definitions.ErrorLoggerFunc
}

func NewUseCasesContainer(store data.Store, logger definitions.LoggerFunc, errorLogger definitions.ErrorLoggerFunc) *UseCasesContainer {
	return &UseCasesContainer{
		store:       store,
		orderUC:     nil,
		webLogger:   logger,
		errorLogger: errorLogger,
	}
}

func (u *UseCasesContainer) GetOrderUseCases() *OrderUseCases {
	if u.orderUC == nil {
		u.orderUC = &OrderUseCases{
			store:  u.store,
			logger: u.webLogger,
			error:  u.errorLogger,
		}
	}
	return u.orderUC
}
