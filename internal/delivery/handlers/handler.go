package handlers

import (
	"applicationDesignTest/internal/entities"
	"applicationDesignTest/internal/usecases"
	"applicationDesignTest/internal/weberrors"
	"net/http"
	"time"
)

type HandlerFunc = func(request *http.Request, usecases *usecases.UseCasesContainer) (interface{}, error)

func MakeOrder(request *http.Request, usecases *usecases.UseCasesContainer) (interface{}, error) {
	query := request.URL.Query()
	bookingRequest := entities.BookingRequest{
		From:      query.Get("from"),
		To:        query.Get("to"),
		UserEmail: query.Get("email"),
		Room:      query.Get("room"),
	}

	if !bookingRequest.Validate() {
		return nil, weberrors.ErrBadRequest
	}

	fromTime, err := time.Parse("2006-01-02", bookingRequest.From)
	if err != nil {
		return nil, weberrors.ErrBadRequest
	}

	toTime, err := time.Parse("2006-01-02", bookingRequest.To)
	if err != nil {
		return nil, weberrors.ErrBadRequest
	}
	booking := entities.Booking{
		From:      fromTime,
		To:        toTime,
		UserEmail: bookingRequest.UserEmail,
		Room:      bookingRequest.Room,
	}
	return usecases.GetOrderUseCases().Book(booking)
}

func GetOrders(request *http.Request, usecases *usecases.UseCasesContainer) (interface{}, error) {
	userEmail := request.URL.Query().Get("email")
	if userEmail == "" {
		return nil, weberrors.ErrBadRequest
	}
	return usecases.GetOrderUseCases().GetByEmail(userEmail)
}
