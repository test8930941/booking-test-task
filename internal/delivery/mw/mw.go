package mw

import (
	"applicationDesignTest/internal/entities"
	"applicationDesignTest/internal/entities/definitions"
	"applicationDesignTest/internal/weberrors"
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type handlerError struct{}

type errorContainer struct {
	value *weberrors.WebError
}

func SetError(ctx context.Context, err error) {
	container := ctx.Value(handlerError{}).(*errorContainer)
	if container != nil {
		e, ok := definitions.ErrorsMap[err]
		if ok {
			container.value = &e
			return
		}
		container.value = &weberrors.WebError{
			Name: err.Error(),
			Code: http.StatusInternalServerError,
		}
	}
}

func AnswerMW(logger definitions.LoggerFunc, errorLogger definitions.ErrorLoggerFunc) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			logger("method %s url %s", r.Method, r.URL.Path)
			container := errorContainer{}

			ctx := r.Context()
			ctx = context.WithValue(ctx, handlerError{}, &container)
			r = r.WithContext(ctx)

			next.ServeHTTP(w, r)

			if container.value != nil {
				errorLogger(container.value)
				responseError(w, container.value)
			}
		})
	}
}

func responseError(w http.ResponseWriter, err *weberrors.WebError) {
	response, marshalErr := json.Marshal(entities.ErrorAnswer{Error: err.Name})
	if marshalErr != nil {
		log.Println("json error marshal error:", marshalErr.Error())
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(err.Code)
	w.Write(response)
}
