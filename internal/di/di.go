package di

import (
	"applicationDesignTest/internal/data"
	"applicationDesignTest/internal/entities/definitions"
	"applicationDesignTest/internal/services"
	"applicationDesignTest/internal/usecases"
)

type DIConfig struct {
	Webaddr     string
	Logger      definitions.LoggerFunc
	ErrorLogger definitions.ErrorLoggerFunc
}

type DI struct {
	config DIConfig

	store data.Store

	webService services.Service
	useCases   *usecases.UseCasesContainer
}

func NewDIContainer(config DIConfig) *DI {
	return &DI{config: config}
}

func (di *DI) Store() data.Store {
	if di.store == nil {
		di.store = data.NewMemoryStore()
	}
	return di.store
}

func (di *DI) WebService() services.Service {
	if di.webService == nil {
		di.webService = services.NewWebService(di.config.Webaddr, di.config.Logger, di.config.ErrorLogger, di.GetUseCases())
	}
	return di.webService
}

func (di *DI) GetUseCases() *usecases.UseCasesContainer {
	if di.useCases == nil {
		di.useCases = usecases.NewUseCasesContainer(di.Store(), di.config.Logger, di.config.ErrorLogger)
	}
	return di.useCases
}
