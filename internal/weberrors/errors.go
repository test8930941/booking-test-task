package weberrors

import (
	"errors"
)

type WebError struct {
	Name string
	Code int
}

func (e WebError) Error() string {
	return e.Name
}

var ErrBadRequest = errors.New("bad request")
var ErrConflict = errors.New("conflict")
