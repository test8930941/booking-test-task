package entities

import (
	"applicationDesignTest/internal/entities/definitions"
	"time"
)

type BookingRequest struct {
	From      string `json:"from" validate:"required"`
	To        string `json:"to" validate:"required"`
	UserEmail string `json:"email" validate:"required"`
	Room      string `json:"room" validate:"required"`
}

func (b BookingRequest) Validate() bool {
	if _, ok := definitions.RoomTypes[b.Room]; !ok {
		return false
	}
	return !(b.From == "" && b.To == "" && b.UserEmail == "")
}

type Booking struct {
	From      time.Time
	To        time.Time
	UserEmail string
	Room      string
}

type Order struct {
	Room      string
	UserEmail string
	From      string
	To        string
}
