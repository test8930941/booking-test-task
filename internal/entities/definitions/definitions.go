package definitions

import (
	"applicationDesignTest/internal/weberrors"
	"net/http"
)

type LoggerFunc = func(format string, v ...any)

type ErrorLoggerFunc = func(error)

var RoomTypes = map[string]struct{}{"econom": {}, "standart": {}, "lux": {}}

var ErrorsMap = map[error]weberrors.WebError{
	weberrors.ErrBadRequest: {"bad request", http.StatusBadRequest},
	weberrors.ErrConflict:   {"conflict", http.StatusConflict},
}
