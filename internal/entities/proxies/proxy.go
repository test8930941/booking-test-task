package proxies

import "time"

type OrderProxy struct {
	Room      string
	UserEmail string
	From      time.Time
	To        time.Time
}
