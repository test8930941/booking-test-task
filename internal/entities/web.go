package entities

type Answer struct {
	Result interface{} `json:"result"`
}

type ErrorAnswer struct {
	Error string `json:"error"`
}
