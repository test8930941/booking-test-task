package services

type Service interface {
	Run() error
	Shutdown() error
}
