package services

import (
	"applicationDesignTest/internal/entities"
	"applicationDesignTest/internal/entities/definitions"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"runtime"

	"applicationDesignTest/internal/delivery/handlers"
	"applicationDesignTest/internal/delivery/mw"
	"applicationDesignTest/internal/usecases"
	"github.com/gorilla/mux"
)

var _ Service = (*webService)(nil)

type webService struct {
	usecases    *usecases.UseCasesContainer
	logger      definitions.LoggerFunc
	errorLogger definitions.ErrorLoggerFunc
	server      *http.Server
}

func NewWebService(addr string, logger definitions.LoggerFunc, errorLogger definitions.ErrorLoggerFunc, usecases *usecases.UseCasesContainer) *webService {
	service := &webService{
		usecases:    usecases,
		logger:      logger,
		errorLogger: errorLogger,
		server: &http.Server{
			Addr: addr,
		},
	}
	return service
}

func (s *webService) Run() error {
	r := mux.NewRouter()
	if err := s.initHandlers(r); err != nil {
		return err
	}
	s.server.Handler = r
	s.logger("server started at %s", s.server.Addr)
	return s.server.ListenAndServe()
}

func (s *webService) Shutdown() error {
	s.logger("Shutting down...")

	if err := s.server.Shutdown(context.Background()); err != nil {
		return err
	}

	s.logger("stopped")

	return nil
}

func (s *webService) initHandlers(r *mux.Router) error {
	r.Use(mw.AnswerMW(s.logger, s.errorLogger))
	r.HandleFunc("/get", s.handle(handlers.GetOrders)).Methods(http.MethodGet)
	r.HandleFunc("/order", s.handle(handlers.MakeOrder)).Methods(http.MethodPost)

	return nil
}

func (s *webService) handle(f handlers.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		funcValue := reflect.ValueOf(f)
		funcName := runtime.FuncForPC(funcValue.Pointer()).Name()
		res, err := f(r, s.usecases)
		if err != nil {
			mw.SetError(r.Context(), err)
			return
		}
		if err := responseResult(w, res); err != nil {
			mw.SetError(r.Context(), err)
		} else {
			s.logger("Method %s was successfully done", funcName)
		}
	}
}

func responseResult(w http.ResponseWriter, result interface{}) error {
	if result == nil {
		result = "OK"
	}
	response, err := json.Marshal(entities.Answer{Result: result})
	if err != nil {
		return fmt.Errorf("json answer marshal error: %w", err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
	return nil
}
